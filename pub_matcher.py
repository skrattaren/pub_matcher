#!/usr/bin/env python3

''' Fuzzy match publishers from list '''
from __future__ import print_function

import sys

from fuzzywuzzy.process import extract as fuzzy_extract

CONTACTED = 'contacted.txt'
SCORE_THRESHOLD = 90


def match_in_list(query, choices):
    ''' Find in list'''
    matches = [m[0] for m in fuzzy_extract(query, choices, limit=3)
               if m[1] >= SCORE_THRESHOLD]
    if matches:
        print("{}: {}".format(query, matches))
        return True
    return False


def main():
    ''' Main wrapper '''
    with open(CONTACTED, 'r') as cnctd_fp:
        contacted = [s.strip() for s in cnctd_fp]
    with open(sys.argv[1], 'r') as to_check_fp:
        to_check = [s.strip() for s in to_check_fp]
    print("=== List of matches ===")
    not_found = [q for q in to_check
                 if not match_in_list(q, contacted)]
    print()
    print("=== Not found ===")
    _ = [print("{}. {}".format(no+1, nf)) for (no, nf) in
         enumerate(sorted(not_found))]


if __name__ == '__main__':
    main()
